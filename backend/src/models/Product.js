const { Schema, model } = require('mongoose');

const ProductScrema = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  brand: {
    type: String, 
    required: true
  },
  description: {
    type: String,
    required: true
  },
  specification: {
    type: Array,
    required: true
  },
  category: {
    type: Array,
    required: true
  }
},{
  timestamps: true
});

module.exports = model('Product', ProductScrema);
