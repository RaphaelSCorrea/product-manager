const { Schema, model } = require('mongoose');

const UsertScrema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  level: {
    type: Number,
    required: true
  },
  password: {
    type: String,
    default: ''
  }
},{
  timestamps: true
});

module.exports = model('Users', UsertScrema);
