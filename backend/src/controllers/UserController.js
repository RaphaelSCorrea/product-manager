const User = require('../models/User');

module.exports = {
  async save(request, response) {
    const Params = request.body

    const user = await User.create(Params);
    
    return response.json(user);
  },
  async get(request, response) {
    const user = await User.find({});
  
    return response.json(user);
  },

  async login(request, response) {
    const { email, password } = request.body;

    const userExists = await User.findOne({email, password});

    const token = {token: '123'};

    if (userExists) {
      return response.status(200).json(token)
    }

    return response.status(403).json({
      message: 'User not exist'
    });
  }
};