const Product = require('../models/Product');

module.exports = {
  async save(request, response) {
    const Params = request.body

    const product = await Product.create(Params);
    
    return response.json(product);
  },
  async get(request, response) {
    const product = await Product.find({});
  
    return response.json(product);
  }
};