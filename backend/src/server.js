const express   = require("express");
const mongoose  = require('mongoose');
const cors      = require('cors');
const Http      = require('http');

const BANCO_URL  = require('./config');
const routes = require('./routes.js');

const app = express();
const server = Http.Server(app);

mongoose.connect( BANCO_URL , {
  useNewUrlParser: true
});

app.use((req, res, next) => {
  //req.connectedUsers = connectedUsers

  return next();
});

app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333);