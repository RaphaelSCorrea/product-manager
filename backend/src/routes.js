const express = require('express');
const Product = require('./controllers/ProductController');
const User = require('./controllers/UserController');


const routes = express.Router();

routes.post('/product/save', Product.save);
routes.get('/product/get', Product.get);

routes.post('/user/save', User.save);
routes.get('/user/get', User.get);
routes.get('/user/login', User.login);

module.exports = routes;
