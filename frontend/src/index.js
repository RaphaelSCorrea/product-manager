import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/login';

import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<App />, document.getElementById('root'));
